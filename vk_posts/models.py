from django.db import models
from django.contrib.auth.models import UserManager as DjangoUserManager, AbstractUser, PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser


GENDER_CHOICES = [
    ("male", u"Мужской"),
    ("female", u"Женский"),
]

RELATIONSHIP_CHOICES = [
    ("none", u"Не выбрано"),
    ("in_a_rel", u"Встречаюсь"),
    ("engaged", u"Помолвлен(а)"),
    ("married", u"Женат/замужем"),
    ("in_love", u"Влюблен(а)"),
    ("complicated", u"Все сложно"),
    ("search", u"В активном поиске"),
]


class UserManager(DjangoUserManager):
    def create_user(self, username, email=None, password=None, **extra_fields):
        user = self.model()
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email=None, password=None, **extra_fields):
        user = self.model(is_staff=True, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    USERNAME_FIELD = 'username'

    email = models.EmailField()
    username = models.CharField(max_length=128, unique=True)
    password = models.CharField(max_length=128)
    is_staff = models.BooleanField(default=False)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=u"Пользователь")
    avatar = models.FileField(verbose_name=u"Аватар", null=True, blank=True)
    bio = models.TextField(max_length=500, null=True, blank=True, verbose_name=u"О себе")
    city = models.CharField(max_length=30, null=True, blank=True, verbose_name=u"Город")
    birth_date = models.DateField(null=True, blank=True, verbose_name=u"Дата рождения")
    gender = models.CharField(max_length=10, verbose_name=u"Пол", choices=GENDER_CHOICES, default="male")
    relationship = models.CharField(max_length=20, verbose_name=u"Статус отношений", choices=RELATIONSHIP_CHOICES,
                                    default="none")


class Post(models.Model):
    datetime = models.DateTimeField(verbose_name=u"Дата", auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=u"Автор", related_name="posts")
    text = models.TextField(max_length=1000, null=True, blank=True, verbose_name=u"Текст")
    image = models.FileField(verbose_name=u"Картинка", null=True, blank=True)

    class Meta:
        ordering = ["-datetime"]
