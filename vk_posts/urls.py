from django.urls import path
from vk_posts.views import HomeView, LogoutView, LoginView, RegisterView, ProfileView, EditProfileView


urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('accounts/login/', LoginView.as_view(), name="login"),
    path('accounts/logout/', LogoutView.as_view(), name="logout"),
    path('accounts/register/', RegisterView.as_view(), name="register"),
    path('accounts/profile/', ProfileView.as_view(), name="profile"),
    path('accounts/profile/edit/', EditProfileView.as_view(), name="edit_profile")
]
