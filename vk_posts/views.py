from django.contrib.auth import logout, authenticate, login
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import TemplateView

from vk_posts.forms import RegisterForm, ProfileForm, PostForm, LoginForm
from vk_posts.models import Post
from vk_posts.services import register_user


class HomeView(TemplateView):
    template_name = "vk_posts/home.html"
    newsfeed_template_name = "vk_posts/newsfeed.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return render(request, self.template_name)

        if request.method == "POST":
            form = PostForm(request.POST, request.FILES)
            if form.is_valid():
                form.instance.author = request.user
                form.save()
                return redirect(reverse("home"))

        context = {
            "posts": Post.objects.all()
        }
        return render(request, self.newsfeed_template_name, context)


class RegisterView(TemplateView):
    template_name = "vk_posts/registration/register.html"

    def dispatch(self, request, *args, **kwargs):
        form = RegisterForm()
        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                username = request.POST['username']
                email = form.cleaned_data['email']
                first_name = form.cleaned_data['first_name']
                last_name = form.cleaned_data['last_name']
                password = request.POST['password']
                register_user(username, email, first_name, last_name, password)
                return redirect(reverse("login"))

        context = {
            "form": form
        }
        return render(request, self.template_name, context)


class LoginView(TemplateView):
    template_name = "vk_posts/registration/login.html"

    def dispatch(self, request, *args, **kwargs):
        form = LoginForm()
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(request, username=username, password=password)

                if user is None:
                    form.add_error(None, 'юзернейм или пароль неверные')
                else:
                    login(request, user)
                    return redirect(reverse('home'))

        context = {
            "form": form
        }
        return render(request, self.template_name, context)


class LogoutView(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return redirect("/")


class ProfileView(TemplateView):
    template_name = "vk_posts/registration/profile.html"

    def dispatch(self, request, *args, **kwargs):
        return render(request, self.template_name)


class EditProfileView(TemplateView):
    template_name = "vk_posts/registration/edit_profile.html"

    def dispatch(self, request, *args, **kwargs):
        form = ProfileForm(instance=self.get_profile(request.user))
        if request.method == "POST":
            form = ProfileForm(request.POST, request.FILES, instance=self.get_profile(request.user))
            if form.is_valid():
                form.instance.user = request.user
                form.save()
                return redirect(reverse("profile"))
        return render(request, self.template_name, {"form": form})

    def get_profile(self, user):
        try:
            return user.profile
        except:
            return None
